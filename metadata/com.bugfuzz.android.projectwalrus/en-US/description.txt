Walrus enables you to use your existing contactless card cloning devices with your Android device. Using a simple interface, cards can be read into a wallet to be written or emulated later.

Designed for physical security assessors, Walrus has features that will help you in your next red team engagement. Bulk card reading lets you easily capture multiple cards as you move around your target location, while simultaneous device reading lets you use different devices to read multiple card types all at once.

Walrus currently supports the Proxmark3 and the Chameleon Mini, with more device support on the way.

NOTE: Walrus requires the use of one of the supported devices; it cannot read or write cards directly through your Android device.

This app is built and signed by Kali NetHunter.
 
